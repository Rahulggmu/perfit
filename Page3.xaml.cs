﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using LightBuzz.Vitruvius;
using System.Data.SqlClient;
using MySql.Data;
//using System.Data.SQLite;
using LightBuzz.Vitruvius.Controls;
using Microsoft.Kinect.Input;
//using Microsoft.Kinect.Wpf;
using MySql.Data.MySqlClient;
//using System.Speech.Recognition;


namespace Perfit_demo
{
    /// <summary>
    /// Interaction logic for Page3.xaml
    /// </summary>
    public partial class Page3 : Page
    {
        
           // InitializeComponent();

            KinectSensor _sensor;

            MultiSourceFrameReader _reader;

            Body body;



            System.Media.SoundPlayer awesome = new System.Media.SoundPlayer();


            System.Media.SoundPlayer proceed = new System.Media.SoundPlayer();


            System.Media.SoundPlayer great = new System.Media.SoundPlayer();


            private void instructionVideo_MediaEnded(object sender, RoutedEventArgs e)
            {

                instructionVideo.Position = TimeSpan.FromSeconds(0);
                instructionVideo.Play();
                //instructionVideo.Position = TimeSpan.Parse("00:00:28");   
            }

            int Count = 0, Ecount = 0, Step_Count = 1, val = 5, exercise_count = 1, flag = 0, flag2 = 0, flag3 = 0, flag4 = 0, flag5 = 0, flag6 = 0;
            float Diff1, Diff2;
            double Knee_R_Height, ShoulderMid_xHeight, Knee_R, Knee_L, Hand_L_Height, Hand_R_Height, Knee_R_Distance, min_distance, Head_Height, SpineMid_Height, SpineMid_xHeight, Knee_Lx_Height, Knee_Rx_Height, Elbow_L_Height, Elbow_R_Height, Wrist_L_Height, Wrist_Lx_Height, Wrist_R_Height, Wrist_Rx_Height, Hip_L_Height, Hip_R_Height;

            double Angle1;
            bool IsAtPosition;




            //LOGIC TO IMPLEMENT JOGIN EXCERSISE

            public void function()
            {
                //finding required distances and angles before hand
                //   LightBuzz.Vitruvius.Controls.KinectAngle n = new LightBuzz.Vitruvius.Controls.KinectAngle(); /*find code for KinectAngle() in Vitruvius Controls
                //  Currently unused*/



                //n.Update(body.Joints[JointType.WristLeft], body.Joints[JointType.ElbowLeft], body.Joints[JointType.ShoulderLeft], 100);
                // Angle1 = (int)n.Angle;

                if (flag2 == 0)
                {
                    flag2 = 1;
                    textbox.Text = (Ecount).ToString();
                    label.Content = "Jog In";
                    instructionVideo.Source = new Uri(@"C:\Users\JBR\Videos\jogin.mp4");
                    instructionVideo.Play();
                    instructionVideo.MediaEnded += new RoutedEventHandler(instructionVideo_MediaEnded);

                }

                switch (Step_Count)
                {

                    case 1:


                        if ((Hand_L_Height < Hip_L_Height) && (Hand_R_Height < Hip_R_Height) && (Count < 4))
                        {
                            Step_Count = 2;



                            goto case 2;
                        }

                        break;

                    case 2:
                        if ((Knee_R - Knee_L) > 0.1f)
                        {
                            //  viewer.Clear();
                            //    viewer.DrawBody(body, 15.0, OkayRun, 10.0, OkayRun);
                            //StatusIndicator.Fill = OkayRun;
                            Step_Count = 3;
                            textbox.Text = (Ecount).ToString();
                            StatusBar.Value += 50;
                            StatusBar2.Value += 10;
                            goto case 3;
                        }
                        break;

                    case 3:
                        if ((Knee_L - Knee_R) > 0.1f)
                        {

                            //  viewer.Clear();
                            //    viewer.DrawBody(body, 15.0, OkayRun, 10.0, OkayRun);
                            //StatusIndicator.Fill = OkayRun;
                            StatusBar.Value += 50;
                            StatusBar.Value = 0;
                            StatusBar2.Value += 10;
                            Count++;
                            Ecount++;
                            textbox.Text = (Ecount).ToString();

                            if (Count == 4)
                            {
                                //Step_Count = 1;
                                Step_Count = 0;
                                flag = 0;
                                Ecount = 0;

                                break;
                            }
                            //StatusBar.Value += 25;
                            Step_Count = 6;
                            goto case 6;
                        }
                        //  
                        //  viewer.Clear();
                        //   viewer.DrawBody(body, 15.0, FailedRun, 10.0, FailedRun);
                        break;

                    case 6:
                        // Count.Text = exercise_count.ToString();
                        {
                            Step_Count = 1;
                            awesome.SoundLocation = "awesome.wav";
                            awesome.Play();
                            /* if (exercise_count == 50)
                             {
                                 Step_Count = 8;
                             }*/
                        }
                        break;
                    default:
                        //   viewer.Clear();
                        //   viewer.DrawBody(body, 15.0, NormalRun, 10.0, NormalRun);
                        // navigater();
                        break;

                }

            }







            //LOGIC TO IMPLEMENT TORSO TWIST EXCERSISE




            public void torso()
            {
                // LightBuzz.Vitruvius.Controls.KinectAngle n = new LightBuzz.Vitruvius.Controls.KinectAngle();

                //n.Update(body.Joints[JointType.WristLeft], body.Joints[JointType.ElbowLeft], body.Joints[JointType.ShoulderLeft], 100);
                //Angle1 = (int)n.Angle;

                //include IsAtPosition in following switch-cases if you want person to stay at min distance from sensor
                /*   if ((Knee_R_Distance * 1000) > min_distance)
                   {
                       IsAtPosition = true;
                   }
                   else IsAtPosition = false;*/
                //********************************** check if offset is needed ***********************
                //Diff1 = (body.Joints[JointType.WristLeft].Position.Y - body.Joints[JointType.ShoulderLeft].Position.Y) * 1000;
                // Diff2 = (body.Joints[JointType.WristRight].Position.Z - body.Joints[JointType.ShoulderRight].Position.Z);
                if (flag4 == 0)
                {
                    flag4 = 1;
                    Step_Count = 1;
                    textbox.Text = (Ecount).ToString();
                    label.Content = "Torso Twists";
                    instructionVideo.Source = new Uri(@"C:\Users\JBR\Videos\torsotwists.mp4");
                    instructionVideo.Play();
                    instructionVideo.MediaEnded += new RoutedEventHandler(instructionVideo_MediaEnded);

                }






                switch (Step_Count)
                {

                    case 1:
                        //StatusBar.Value = 0;

                        if ((Wrist_L_Height > SpineMid_Height))

                        {

                            // viewer.Clear();
                            // viewer.DrawBody(body, 15.0, FailedRun, 10.0, FailedRun);
                            //StatusIndicator.Fill = OkayRun;
                            Step_Count = 2;
                            textbox.Text = (Ecount).ToString();
                            //StatusBar.Value += 33.33;
                            // IsFirstTime = true;

                            goto case 2;

                            //  && (Diff1 > 1) && (Diff2 < 1) && (Diff1 > -100) && (Diff1 < 80))


                        }

                        //  viewer.Clear();
                        //   viewer.DrawBody(body, 15.0, OkayRun, 10.0, OkayRun);
                        break;
                    case 2:
                        if ((Wrist_Lx_Height > SpineMid_xHeight) && (Wrist_Rx_Height < SpineMid_xHeight))
                        {
                            //  viewer.Clear();

                            //    viewer.DrawBody(body, 15.0, OkayRun, 10.0, OkayRun);
                            //StatusIndicator.Fill = OkayRun;
                            Step_Count = 3;
                            textbox.Text = (Ecount).ToString();
                            StatusBar.Value += 50;
                            StatusBar2.Value += 10;
                            goto case 3;
                        }
                        break;
                    case 3:
                        if ((Wrist_Lx_Height < SpineMid_xHeight) && (Wrist_Rx_Height > SpineMid_xHeight))
                        {

                            //  viewer.Clear();
                            StatusBar.Value += 50;
                            StatusBar.Value = 0;
                            StatusBar2.Value += 10;
                            Count++;
                            Ecount++;

                            //    viewer.DrawBody(body, 15.0, OkayRun, 10.0, OkayRun);
                            //StatusIndicator.Fill = OkayRun;
                            Step_Count = 3;
                            textbox.Text = (Ecount).ToString();
                            if (Count == 8)
                            {
                                Step_Count = 0;
                                flag = 0;
                                Ecount = 0;

                                break;
                            }
                            // StatusBar.Value += 33.33;
                            goto case 6;
                        }
                        //  
                        //  viewer.Clear();
                        //   viewer.DrawBody(body, 15.0, FailedRun, 10.0, FailedRun);
                        break;
                    case 6:

                        Step_Count = 1;
                        proceed.SoundLocation = "proceed.wav";
                        proceed.Play();

                        // exercise_count++;
                        break;
                    default:
                        //   viewer.Clear();
                        //   viewer.DrawBody(body, 15.0, NormalRun, 10.0, NormalRun);
                        //navigater();
                        break;

                }
            }




            //LOGIC TO IMPLEMENT ROW AND LATERAL



            public void rowlateral()
            {
                // LightBuzz.Vitruvius.Controls.KinectAngle n = new LightBuzz.Vitruvius.Controls.KinectAngle();

                //n.Update(body.Joints[JointType.WristLeft], body.Joints[JointType.ElbowLeft], body.Joints[JointType.ShoulderLeft], 100);
                //Angle1 = (int)n.Angle;

                //include IsAtPosition in following switch-cases if you want person to stay at min distance from sensor

                //********************************** check if offset is needed ***********************
                // Diff1 = (body.Joints[JointType.WristLeft].Position.Y - body.Joints[JointType.ShoulderLeft].Position.Y) * 1000;
                // Diff2 = (body.Joints[JointType.WristRight].Position.Z - body.Joints[JointType.ShoulderRight].Position.Z);

                if (flag5 == 0)
                {
                    flag5 = 1;
                    Step_Count = 1;
                    label.Content = "Row and Lateral";
                    textbox.Text = (Ecount).ToString();
                    instructionVideo.Source = new Uri(@"C:\Users\JBR\Videos\rowlateral.mp4");
                    instructionVideo.Play();
                    instructionVideo.MediaEnded += new RoutedEventHandler(instructionVideo_MediaEnded);

                }


                switch (Step_Count)
                {

                    case 1:
                        //StatusBar.Value = 0;

                        // StatusBar.Value = 0;
                        // viewer.Clear();
                        // viewer.DrawBody(body, 15.0, FailedRun, 10.0, FailedRun);
                        //StatusIndicator.Fill = OkayRun;
                        if ((Knee_Lx_Height > Knee_Rx_Height) && (Wrist_L_Height > SpineMid_Height))
                        {

                            //&& (Wrist_R_Height > SpineMid_Height) && (Wrist_Rx_Height > SpineMid_xHeight) && (Wrist_Lx_Height > SpineMid_xHeight)
                            // viewer.Clear();
                            // viewer.DrawBody(body, 15.0, FailedRun, 10.0, FailedRun);
                            //StatusIndicator.Fill = OkayRun;
                            Step_Count = 2;
                            textbox.Text = (Ecount).ToString();
                            //StatusBar.Value += 25;
                            //  IsFirstTime = true;
                            StatusBar.Value += 50;
                            StatusBar2.Value += 10;

                            goto case 2;

                        }

                        //  viewer.Clear();
                        //   viewer.DrawBody(body, 15.0, OkayRun, 10.0, OkayRun);
                        break;
                    case 2:
                        if ((Knee_Lx_Height < Knee_Rx_Height) && (Wrist_L_Height > SpineMid_Height))
                        {
                            //  viewer.Clear();
                            //    viewer.DrawBody(body, 15.0, OkayRun, 10.0, OkayRun);
                            //StatusIndicator.Fill = OkayRun;
                            StatusBar.Value += 50;
                            StatusBar.Value = 0;
                            StatusBar2.Value += 10;
                            Step_Count = 3;
                            Count++;
                            Ecount++;
                            textbox.Text = (Ecount).ToString();
                            if (Count == 12)
                            {
                                Step_Count = 0;
                                flag = 0;
                                Ecount = 0;

                                break;
                            }
                            //StatusBar.Value += 25;
                            goto case 6;
                        }
                        break;
                    /*  case 3: if (Wrist_L_Height > SpineMid_Height)
                          {
                              //  viewer.Clear();
                              //    viewer.DrawBody(body, 15.0, OkayRun, 10.0, OkayRun);
                              //StatusIndicator.Fill = OkayRun;
                              Step_Count = 4;
                              Count++;
                              Ecount++;
                              textbox.Text = (Ecount).ToString();
                              if (Count == 11)
                              {
                                  Step_Count = 1;
                                  flag = 0;
                                  Ecount = 0;
                                  break;
                              }
                             // StatusBar.Value += 25;
                              goto case 4;
                          }
                          //  
                          //  viewer.Clear();
                          //   viewer.DrawBody(body, 15.0, FailedRun, 10.0, FailedRun);
                          break;
                      case 4: if (Wrist_L_Height > SpineMid_Height)
                          {


                          //  viewer.Clear();

                              //    viewer.DrawBody(body, 15.0, OkayRun, 10.0, OkayRun);
                              //StatusIndicator.Fill = OkayRun;
                              Step_Count = 6;
                             // StatusBar.Value += 25;
                              goto case 6;
                          }
                          //  
                          //  viewer.Clear();
                          //   viewer.DrawBody(body, 15.0, FailedRun, 10.0, FailedRun);
                          break;
                     * */
                    case 6:

                        Step_Count = 1;
                        great.SoundLocation = "great.wav";
                        great.Play();
                        // exercise_count++;
                        break;
                    default:
                        //   viewer.Clear();
                        //   viewer.DrawBody(body, 15.0, NormalRun, 10.0, NormalRun);
                        //navigater();
                        break;

                }
            }




            //PROGRAM FOR SQUATS





            public void squats()
            {
                // LightBuzz.Vitruvius.Controls.KinectAngle n = new LightBuzz.Vitruvius.Controls.KinectAngle();

                //n.Update(body.Joints[JointType.WristLeft], body.Joints[JointType.ElbowLeft], body.Joints[JointType.ShoulderLeft], 100);
                // Angle1 = (int)n.Angle;

                //include IsAtPosition in following switch-cases if you want person to stay at min distance from sensor

                //********************************** check if offset is needed ***********************
                //Diff1 = (body.Joints[JointType.WristLeft].Position.Y - body.Joints[JointType.ShoulderLeft].Position.Y) * 1000;
                // Diff2 = (body.Joints[JointType.WristRight].Position.Z - body.Joints[JointType.ShoulderRight].Position.Z);


                if (flag6 == 0)
                {
                    flag6 = 1;
                    Step_Count = 1;
                    textbox.Text = (Ecount).ToString();
                    label.Content = "Squats";
                    instructionVideo.Source = new Uri(@"C:\Users\JBR\Videos\squats.mp4");
                    instructionVideo.Play();
                    instructionVideo.MediaEnded += new RoutedEventHandler(instructionVideo_MediaEnded);

                }

                switch (Step_Count)
                {

                    case 1:// StatusBar.Value = 0;
                        if ((Hip_R_Height > Knee_R_Height)) //checks if user at marked position
                        {

                            // viewer.Clear();
                            // viewer.DrawBody(body, 15.0, FailedRun, 10.0, FailedRun);
                            //StatusIndicator.Fill = OkayRun;
                            Step_Count = 2;

                            // StatusBar.Value += 33.33;
                            // IsFirstTime = true;





                        }
                        //  viewer.Clear();
                        //   viewer.DrawBody(body, 15.0, OkayRun, 10.0, OkayRun);
                        break;
                    case 2:
                        if ((Wrist_L_Height > SpineMid_Height))
                        {
                            //  viewer.Clear();
                            //    viewer.DrawBody(body, 15.0, OkayRun, 10.0, OkayRun);
                            //StatusIndicator.Fill = OkayRun;
                            Step_Count = 3;
                            StatusBar.Value += 50;
                            StatusBar2.Value += 10;
                            //StatusBar.Value += 33.33;
                            goto case 3;
                        }
                        break;
                    case 3:
                        if (Hip_R_Height < Knee_R_Height)
                        {
                            Count++;
                            Ecount++;
                            StatusBar.Value += 50;
                            StatusBar.Value = 0;
                            StatusBar2.Value += 10;
                            textbox.Text = (Ecount).ToString();
                            //    viewer.DrawBody(body, 15.0, OkayRun, 10.0, OkayRun);
                            //StatusIndicator.Fill = OkayRun;
                            Step_Count = 6;
                            // StatusBar.Value += 33.33;
                            goto case 6;
                        }
                        //  
                        //  viewer.Clear();
                        //   viewer.DrawBody(body, 15.0, FailedRun, 10.0, FailedRun);
                        break;
                    case 6:

                        Step_Count = 1;


                        break;
                    default:
                        //   viewer.Clear();
                        //   viewer.DrawBody(body, 15.0, NormalRun, 10.0, NormalRun);

                        break;

                }
            }



            public void Page1()
            {
                InitializeComponent();




                //  (StatusBar2.Value) = globalclass.statusval;
                //  globalclass.fullcount = 0;
                _sensor = KinectSensor.GetDefault();
                if (_sensor != null)
                {
                    _sensor.Open();

                    _reader = _sensor.OpenMultiSourceFrameReader(FrameSourceTypes.Color | FrameSourceTypes.Depth | FrameSourceTypes.Infrared | FrameSourceTypes.Body);
                    _reader.MultiSourceFrameArrived += _reader_MultiSourceFrameArrived;

                    /*_playersController = new PlayersController();
                    _playersController.BodyEntered += _playersController_BodyEntered;
                    _playersController.BodyLeft += _playersController_BodyLeft;
                    _playersController.Start();*/
                }

            }

            void _reader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
            {


                var reference = e.FrameReference.AcquireFrame();

                // Color
                using (var frame = reference.ColorFrameReference.AcquireFrame())
                {
                    if (frame != null)
                    {
                        viewer.Image = frame.ToBitmap();

                    }
                }

                //body
                using (var frame = reference.BodyFrameReference.AcquireFrame())
                {
                    if (frame != null)
                    {
                        var bodies = frame.Bodies();
                        body = bodies.Closest();
                        viewer.DrawBody(body);
                    }





                    if (body != null)
                    {


                        Knee_R_Distance = body.Joints[JointType.KneeRight].Position.Z;
                        min_distance = 2000;
                        Head_Height = body.Joints[JointType.Head].Position.Y;
                        SpineMid_Height = body.Joints[JointType.SpineMid].Position.Y;
                        SpineMid_xHeight = body.Joints[JointType.SpineMid].Position.Z;
                        Elbow_L_Height = body.Joints[JointType.ElbowLeft].Position.Y;
                        Elbow_R_Height = body.Joints[JointType.ElbowRight].Position.Y;
                        Wrist_L_Height = body.Joints[JointType.WristLeft].Position.Y;
                        Wrist_Lx_Height = body.Joints[JointType.WristLeft].Position.Z;
                        Wrist_R_Height = body.Joints[JointType.WristRight].Position.Y;
                        Wrist_Rx_Height = body.Joints[JointType.WristRight].Position.Z;
                        Hip_L_Height = body.Joints[JointType.HipLeft].Position.Y;
                        Hip_R_Height = body.Joints[JointType.HipRight].Position.Y;
                        Hand_L_Height = body.Joints[JointType.HandLeft].Position.Y;
                        Hand_R_Height = body.Joints[JointType.HandRight].Position.Y;
                        Knee_R = body.Joints[JointType.KneeRight].Position.Y;
                        Knee_L = body.Joints[JointType.KneeLeft].Position.Y;
                        Knee_Rx_Height = body.Joints[JointType.KneeRight].Position.Z;
                        Knee_Lx_Height = body.Joints[JointType.KneeLeft].Position.Z;
                        ShoulderMid_xHeight = body.Joints[JointType.Neck].Position.Z;
                        Knee_R_Height = body.Joints[JointType.KneeRight].Position.Y;






                        if (Count == 0 && flag == 0)
                        {
                            flag = 1;

                            instructionVideo.Source = new Uri(@"C:\Users\JBR\Videos\staticloop.mp4");
                            instructionVideo.Play();
                            instructionVideo.MediaEnded += new RoutedEventHandler(instructionVideo_MediaEnded);
                        }
                        else if (Count == 4 && flag == 0)
                        {
                            flag = 1;
                            instructionVideo.Source = new Uri(@"C:\Users\JBR\Videos\staticloop.mp4");
                            instructionVideo.Play();
                            instructionVideo.MediaEnded += new RoutedEventHandler(instructionVideo_MediaEnded);
                        }
                        else if (Count == 8 && flag == 0)
                        {
                            flag = 1;
                            instructionVideo.Source = new Uri(@"C:\Users\JBR\Videos\staticloop.mp4");
                            instructionVideo.Play();
                            instructionVideo.MediaEnded += new RoutedEventHandler(instructionVideo_MediaEnded);
                        }
                        else if (Count == 12 && flag == 0)
                        {
                            flag = 1;
                            instructionVideo.Source = new Uri(@"C:\Users\JBR\Videos\staticloop.mp4");
                            instructionVideo.Play();
                            instructionVideo.MediaEnded += new RoutedEventHandler(instructionVideo_MediaEnded);
                        }




                        /*

                                            else if (Count > 0 && Count < 3 && flag3==0)
                                            {
                                                flag3 = 1;
                                                instructionVideo.Source = new Uri(@"C:\Users\JBR\Videos\jogin.mp4");
                                                instructionVideo.Play();
                                                instructionVideo.MediaEnded += new RoutedEventHandler(instructionVideo_MediaEnded);
                                            }

                                            else if (Count > 3 && Count < 7 && flag3 == 0)
                                            {
                                                flag3 = 1;
                                                instructionVideo.Source = new Uri(@"C:\Users\JBR\Videos\torsotwists.mp4");
                                                instructionVideo.Play();
                                                instructionVideo.MediaEnded += new RoutedEventHandler(instructionVideo_MediaEnded);
                                            }
                                            else if (Count > 7 && Count < 11 && flag3 == 0)
                                            {
                                                flag3 = 1;
                                                instructionVideo.Source = new Uri(@"C:\Users\JBR\Videos\rowlateral.mp4");
                                                instructionVideo.Play();
                                                instructionVideo.MediaEnded += new RoutedEventHandler(instructionVideo_MediaEnded);
                                            }
                        */









                        if (Count == 0)
                        {




                            var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(4) };
                            timer.Start();
                            timer.Tick += (sende, args) =>
                            {

                                function();
                                timer.Stop();
                            };


                        }

                        else if (Count > 0 && Count < 4)
                        {

                            function();
                        }





                        else if (Count == 4)
                        {

                            var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(4) };
                            timer.Start();
                            timer.Tick += (sendea, args) =>
                            {

                                torso();
                                timer.Stop();
                            };

                        }

                        else if (Count == 8)
                        {

                            var timer2 = new DispatcherTimer { Interval = TimeSpan.FromSeconds(4) };
                            timer2.Start();
                            timer2.Tick += (sendes, args) =>
                            {

                                rowlateral();
                                timer2.Stop();
                            };

                        }

                        else if (Count > 4 && Count < 8)
                        {
                            torso();
                        }







                        else if (Count > 8 && Count < 12)
                        {
                            rowlateral();
                        }





                        else if (Count == 12)
                        {

                            var timer3 = new DispatcherTimer { Interval = TimeSpan.FromSeconds(4) };
                            timer3.Start();
                            timer3.Tick += (sended, args) =>
                            {

                                squats();
                                timer3.Stop();
                            };
                        }
                        else if (Count > 12)
                        {
                            squats();
                        }

                    }
                }
            }

        }
    }
