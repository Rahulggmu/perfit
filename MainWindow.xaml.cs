﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Perfit_demo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            MainFrame.Navigate(new Uri("Page11.xaml", UriKind.Relative));
            //MainFrame.Content = new Page1();
        }

        private void MainFrame_Navigated(object sender, NavigationEventArgs e)
        {
        }

    }



    public static class globalclass
    {
        public static int qr { get; set; }
    }
}
